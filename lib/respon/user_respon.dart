import 'dart:convert';

import 'package:getapiproject/model/user.dart';
import 'package:http/http.dart' as http;

Future<Stream<User>> getUser(String url) async{
  final baseUrl = url;
  final client = new http.Client();
  final String headers = "";
  final streamedRest = await client.send(http.Request('get', Uri.parse(baseUrl)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .expand((data) => (data as List))
      .map((data) => User.fromJSON(data));

  //co gi hot :)))
}
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getapiproject/model/user.dart';
import 'package:getapiproject/respon/user_respon.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
String baseUrl = 'https://api.github.com/users?since=';
class _HomeState extends State<Home> {
  List<User> _users = <User>[];
  @override
  void initState() {
    // TODO: implement initState
    listenForUser(baseUrl);
    super.didChangeDependencies();
  }

  // Goi Data tu Stream len ListView
  void listenForUser(String url) async {
    final Stream<User> stream = await getUser(url);
    stream.listen((User user) => setState(() => _users.add(user)));
  }
  void clickLoadMore(){
    String url ;
    for(int i = 1 ; i<= 10 ; i++)
      {
        url = baseUrl + "${_users.last.id}";
        listenForUser(url);
      }
  }
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            new IconButton(icon:const Icon(Icons.file_download), onPressed: clickLoadMore)
          ],
          centerTitle: true,
          title: Text('Manager User'),
        ),
        // ham duyet users , tai context theo index , tra ve user theo index
        body: ListView.builder(
          itemCount: _users.length,
          itemBuilder: (context, index) => UserTile(_users[index]),
        ),
  );
}

class UserTile extends StatelessWidget {
  final User _users;
  UserTile(this._users);
  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          ListTile(
            title: Text(_users.login),
            subtitle: Text(_users.node_id),
            leading: Container(
                margin: EdgeInsets.only(left: 6.0),
                child: Image.network(
                  _users.avatar_url,
                  height: 50.0,
                  fit: BoxFit.fill,
                )),
          ),
          Divider()
        ],
      );
}

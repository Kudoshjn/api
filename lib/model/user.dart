class User {
  final int id;
  final String login;
  final String node_id;
  final String avatar_url;

  User.fromJSON(Map<String, dynamic> jsonMap) :
        id = jsonMap['id'],
        login = jsonMap['login'],
        node_id = jsonMap['node_id'],
        avatar_url = jsonMap['avatar_url'];
}